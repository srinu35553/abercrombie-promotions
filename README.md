# ABERCORMBIE-PROMOTIONS #

This is a sample app using the feed from abercrombie (www.abercrombie.com/anf/nativeapp/Feeds/promotions.json) and showing the promotions to the user.

## THIRD-PARTY LIBRARIES ##

This app uses third party libraries like Retrofit and Picasso. Retrofit is used for all API calls and also parses the JSON response into Java objects.
Picasso helps in loading images into ImageViews and also caches the images.

## TESTING ##

Espresso and JUnit are used for writing test cases. I have written very minimal test cases given the time constraint. But, I believe I have showcased more than enough to prove that I know how to test my code and most importantly how to work in a TDD environment ;)

## STORAGE ##

The API data is stored locally on the device using SQLite database which is updated for every app launch. (I haven't used ContentResolver or Content Providers for the same reason of time constraint, but rest assured that I am of course going to use Content resolvers and providers for the real world applications :) )

## VERSION CONTROL ##

Git is used for version control. (Each feature is developed on a separate branch and merged PRs into development and finally for the release version, the code is merged from development into master)

## HOW TO RUN ##

Download the source code or clone/fork the repository and build it using Gradle in Android Studio. I have used gradle-2.10.
Run the app on any device running Android OS KitKat and higher or run on an emulator using API 19 or higher.

That's it! Enjoy the app!