package com.example.afpromotions.utils;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.afpromotions.R;
import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.database.Database;
import com.example.afpromotions.fragments.DetailFragment;
import com.example.afpromotions.fragments.LaunchFragment;
import com.example.afpromotions.fragments.WebViewFragment;

import java.util.List;

public class FragmentTestContainerActivity extends AppCompatActivity implements DialogInterface.OnClickListener{

    private static final int CONTAINER_ID = 1;
    public List<Promotion> promotions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Database database = new Database(this);
        database.open();

        promotions = database.getPromotions();

        FrameLayout.LayoutParams params =
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(CONTAINER_ID);

        setContentView(frameLayout, params);
    }

    public void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(CONTAINER_ID, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    // Method to handle clicks on the promotions and buttons within fragments
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shop_now:
                if(GetConnectivity.isNetworkAvailable(this)){
                    WebViewFragment webViewFragment = new WebViewFragment();
                    Bundle args = new Bundle();
                    args.putString(WebViewFragment.URL, promotions.get(0).getButton().get(0).getTarget());
                    webViewFragment.setArguments(args);
                    getSupportFragmentManager().beginTransaction()
                            .replace(CONTAINER_ID, webViewFragment, WebViewFragment.TAG)
                            .commit();
                } else {
                    Dialog.showErrorDialog(
                            getString(R.string.no_internet_header),
                            getString(R.string.no_internet_description),
                            getString(R.string.close),
                            getString(R.string.close), // This won't be displayed
                            true,
                            this,
                            this
                    );
                }
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEUTRAL:
                dialog.dismiss();
                break;
        }
    }
}
