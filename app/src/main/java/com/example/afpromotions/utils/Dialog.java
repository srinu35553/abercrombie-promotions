package com.example.afpromotions.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;

import com.example.afpromotions.R;

public class Dialog {

    // Error dialog
    public static void showErrorDialog(String title, String description, String actionText, String cancelText,
                                 boolean isOnlyAction, Context context, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder((new ContextThemeWrapper(context, R.style.AlertDialogCustom)));

        // Setting Dialog Title
        builder.setTitle(title);

        // Setting Dialog Message
        builder.setMessage(description);

        builder.setIcon(R.drawable.failure);

        if (!isOnlyAction) {
            // Setting OK Button
            builder.setPositiveButton(actionText, listener);

            // Setting Cancel Button
            builder.setNegativeButton(cancelText, listener);
        } else {
            // Setting neutral button
            builder.setNeutralButton(actionText, listener);
        }

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }
}
