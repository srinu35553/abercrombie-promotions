package com.example.afpromotions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;

import com.example.afpromotions.api.Access;
import com.example.afpromotions.api.model.ApiResponse;
import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.database.Database;
import com.example.afpromotions.utils.Dialog;
import com.example.afpromotions.utils.GetConnectivity;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Activity with a blank layout which displays the branding of the app
 * as it loads. This is where the internet connectivity is checked and api call is done.
 */
public class SplashScreen extends AppCompatActivity implements Callback<ApiResponse>, DialogInterface.OnClickListener {

    private static final String TAG = SplashScreen.class.getSimpleName();

    private Database database;
    private ArrayList<Promotion> promotions;
    private boolean offline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = new Database(this);
        database.open();

        if (database.getPromotions().size() > 0) {
            offline = true;
        }
    }

    @Override
    protected void onResume() {
        database.open();
        fetch();
        super.onResume();
    }

    @Override
    protected void onPause() {
        database.close();
        super.onPause();
    }

    @Override
    public void success(ApiResponse apiResponse, Response response) {
        if (apiResponse != null && apiResponse.getPromotions() != null) {
            promotions = new ArrayList<>();
            promotions.addAll(apiResponse.getPromotions());
            database.open();
            database.clearDatabase();
            database.addPromotions(promotions);
            startApp();
        }
    }

    @Override
    public void failure(RetrofitError error) {
        Dialog.showErrorDialog(
                getString(R.string.server_error),
                getString(R.string.server_error_description) + "\n" + error.getLocalizedMessage(),
                getString(R.string.okay),
                getString(R.string.okay), // This won't be displayed
                true,
                this,
                this
        );
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                fetch();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }
    }

    // Method to fetch api data
    private void fetch() {
        if (GetConnectivity.isNetworkAvailable(this)) {
            Access.getInstance().getApi().getPromotions(this);
        } else if (!offline) {
            Dialog.showErrorDialog(
                    getString(R.string.no_internet_header),
                    getString(R.string.no_internet_description),
                    getString(R.string.try_again),
                    getString(R.string.close),
                    false,
                    this,
                    this
            );
        } else if (offline) {
            startApp();
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(intent);
        database.close();
        finish();
    }
}
