package com.example.afpromotions;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.database.Database;
import com.example.afpromotions.fragments.DetailFragment;
import com.example.afpromotions.fragments.LaunchFragment;
import com.example.afpromotions.fragments.WebViewFragment;
import com.example.afpromotions.utils.Dialog;
import com.example.afpromotions.utils.GetConnectivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DialogInterface.OnClickListener{

    public static final String TAG = MainActivity.class.getSimpleName();

    private TextView title;
    private ImageView backButton;

    private LaunchFragment launchFragment;
    private DetailFragment detailFragment;
    private WebViewFragment webViewFragment;

    private Database database;
    private ArrayList<Promotion> promotions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = new Database(this);
        database.open();

        promotions = database.getPromotions();

        Typeface font = Typeface.createFromAsset(getAssets(), "garamond.ttf");
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(font);

        backButton = (ImageView) findViewById(R.id.back_button);

        if (savedInstanceState == null) {
            launchFragment = new LaunchFragment();
            detailFragment = new DetailFragment();
            webViewFragment = new WebViewFragment();

            launchFragment.setData(promotions);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_frame, launchFragment, LaunchFragment.TAG)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        database.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        database.close();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEUTRAL:
                dialog.dismiss();
                break;
        }
    }

    // Method to handle clicks on the promotions and buttons within fragments
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.promotion_row1:
                backButton.setVisibility(View.VISIBLE);
                detailFragment.setData(promotions.get(0));
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(LaunchFragment.TAG)
                        .replace(R.id.fragment_frame, detailFragment, DetailFragment.TAG)
                        .commit();
                break;
            case R.id.promotion_row2:
                backButton.setVisibility(View.VISIBLE);
                detailFragment.setData(promotions.get(1));
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(LaunchFragment.TAG)
                        .replace(R.id.fragment_frame, detailFragment, DetailFragment.TAG)
                        .commit();
                break;
            case R.id.shop_now:
                if(GetConnectivity.isNetworkAvailable(this)){
                    Bundle args = new Bundle();
                    args.putString(WebViewFragment.URL, detailFragment.shopNowUrl);
                    webViewFragment.setArguments(args);
                    getSupportFragmentManager().beginTransaction()
                            .addToBackStack(DetailFragment.TAG)
                            .replace(R.id.fragment_frame, webViewFragment, WebViewFragment.TAG)
                            .commit();
                } else {
                    Dialog.showErrorDialog(
                            getString(R.string.no_internet_header),
                            getString(R.string.no_internet_description),
                            getString(R.string.close),
                            getString(R.string.close), // This won't be displayed
                            true,
                            this,
                            this
                    );
                }
                break;
            case R.id.back_button:
                goBack(true);
                break;
        }
    }

    private void goBack(boolean goBackInActivity) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 2 && !goBackInActivity) {
                if (webViewFragment.canGoBack()) {
                    webViewFragment.goBack();
                } else {
                    getSupportFragmentManager().popBackStack();
                }
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    backButton.setVisibility(View.GONE);
                }
                getSupportFragmentManager().popBackStack();
            }
        }
    }
}
