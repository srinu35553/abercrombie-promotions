package com.example.afpromotions.api.model;

import java.util.ArrayList;
import java.util.List;

public class ApiResponse {
    private List<Promotion> promotions = new ArrayList<Promotion>();

    public List<Promotion> getPromotions() {
        return promotions;
    }
}
