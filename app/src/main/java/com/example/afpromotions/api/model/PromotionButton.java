package com.example.afpromotions.api.model;

public class PromotionButton {
    private String target;
    private String title;

    public void setTarget(String target) {
        this.target = target;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public String getTitle() {
        return title;
    }
}
