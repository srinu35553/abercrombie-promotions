package com.example.afpromotions.api.model;

import java.util.ArrayList;
import java.util.List;

public class Promotion {
    private List<PromotionButton> button = new ArrayList<PromotionButton>();
    private String description;
    private String footer;
    private String image;
    private String title;
    private int _id;

    public List<PromotionButton> getButton() {
        return button;
    }

    public void setButton(List<PromotionButton> button) {
        this.button = button;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
}
