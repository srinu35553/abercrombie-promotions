package com.example.afpromotions.api;

import com.example.afpromotions.api.model.ApiResponse;

import retrofit.Callback;
import retrofit.http.GET;

public interface AbercrombieApi {
    @GET("/anf/nativeapp/Feeds/promotions.json")
    void getPromotions(
            Callback<ApiResponse> callback
    );
}
