package com.example.afpromotions.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.afpromotions.R;
import com.example.afpromotions.api.model.Promotion;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class LaunchFragment extends Fragment {

    public static final String TAG = LaunchFragment.class.getSimpleName();
    private View rootView;
    private TableLayout promotionTable;

    private ImageView promotionImage1;
    private TextView promotionTitle1;
    private ImageView promotionImage2;
    private TextView promotionTitle2;

    private List<Promotion> promotions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_launch, container, false);
        promotionTable = (TableLayout) rootView.findViewById(R.id.promotion_table);

        promotionImage1 = (ImageView) promotionTable.findViewById(R.id.promotion_image1);
        promotionTitle1 = (TextView) promotionTable.findViewById(R.id.promotion_title1);
        promotionImage2 = (ImageView) promotionTable.findViewById(R.id.promotion_image2);
        promotionTitle2 = (TextView) promotionTable.findViewById(R.id.promotion_title2);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (promotions != null) {
            inflate();
        }
    }

    // Method to inflate views
    private void inflate() {
        Promotion promotion1 = promotions.get(0);
        Promotion promotion2 = promotions.get(1);

        if (promotion1 != null) {
            Picasso.with(getActivity())
                    .load(promotion1.getImage())
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(promotionImage1);
            promotionTitle1.setText(promotion1.getTitle());
        }
        if (promotion2 != null) {
            Picasso.with(getActivity())
                    .load(promotion2.getImage())
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(promotionImage2);
            promotionTitle2.setText(promotion2.getTitle());
        }
    }

    /**
     * Method to set the data from API into the local promotions list
     * called from {@link com.example.afpromotions.MainActivity}
     */
    public void setData(List<Promotion> promotions) {
        if (promotions != null) {
            this.promotions = new ArrayList<>();
            this.promotions.addAll(promotions);
        }
    }
}
