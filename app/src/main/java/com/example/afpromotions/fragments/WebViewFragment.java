package com.example.afpromotions.fragments;

import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.afpromotions.R;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;

/**
 * Fragment to display the web view
 */
public class WebViewFragment extends Fragment {

    public static final String TAG = WebViewFragment.class.getSimpleName();
    public static final String URL = "url";
    private View rootView;
    private WebView browser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
        browser = (WebView) rootView.findViewById(R.id.web_view);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // To resume loading
        browser.onResume();
        browser.resumeTimers();
        Bundle args = getArguments();
        if (args != null) {
            browser.getSettings().setBuiltInZoomControls(true);
            browser.getSettings().setDisplayZoomControls(false);
            browser.getSettings().setJavaScriptEnabled(true);
            browser.getSettings().setAppCacheEnabled(true);
            browser.setWebViewClient(new BrowserClient());
            browser.loadUrl(args.getString(URL));
        }
    }

    // Method to check if back navigation is possible for the webview
    public boolean canGoBack() {
        return browser.canGoBack();
    }

    // It is called when the webview has a history to go back to
    public void goBack() {
        browser.goBack();
    }

    @Override
    public void onPause() {
        super.onPause();
        // To pause the loading
        browser.onPause();
        browser.pauseTimers();
        browser.clearCache(true);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // To stop all loading
        browser.loadUrl("about:blank");
        browser.stopLoading();
        browser.clearCache(true);
        browser.setWebViewClient(null);
        browser.destroy();
        browser = null;
    }

    // The {@link WebViewClient} class to handle simple browser
    // functionality within the app's WebView
    private class BrowserClient extends WebViewClient {

        private Intent newEmailIntent(String address, String subject, String body, String cc) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:")); // only email apps should handle this
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            emailIntent.putExtra(Intent.EXTRA_TEXT, body);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(Intent.EXTRA_CC, cc);
            return emailIntent;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.getSettings().setBuiltInZoomControls(true);
            view.getSettings().setDisplayZoomControls(false);
            view.getSettings().setJavaScriptEnabled(true);
            view.getSettings().setAppCacheEnabled(true);
            if (url != null) {
                if (url.startsWith("mailto:")) {
                    MailTo mailTo = MailTo.parse(url);
                    Intent emailIntent = newEmailIntent(mailTo.getTo(), mailTo.getSubject(), mailTo.getBody(), mailTo.getCc());
                    if (emailIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_email_pretext)));
                    }
                    return true;
                } else {
                    view.loadUrl(url);
                }
            }
            return true;
        }
    }
}
