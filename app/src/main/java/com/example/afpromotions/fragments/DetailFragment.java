package com.example.afpromotions.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.afpromotions.R;
import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.utils.Dialog;
import com.example.afpromotions.utils.GetConnectivity;
import com.squareup.picasso.Picasso;

import java.io.InputStream;

public class DetailFragment extends Fragment {

    public static final String TAG = DetailFragment.class.getSimpleName();
    private View rootView;

    private ImageView promotionImage;
    private TextView promotionTitle;
    private TextView promotionDescription;
    private TextView promotionFooter;
    private Button shopNow;

    public String shopNowUrl;
    private Promotion promotion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        promotionImage = (ImageView) rootView.findViewById(R.id.promotion_image);
        promotionTitle = (TextView) rootView.findViewById(R.id.promotion_title);
        promotionDescription = (TextView) rootView.findViewById(R.id.promotion_description);
        promotionFooter = (TextView) rootView.findViewById(R.id.promotion_footer);
        shopNow = (Button) rootView.findViewById(R.id.shop_now);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Setting the data to the views in onResume()
        inflate();
    }

    /**
     * Method to set the data from API into the local promotions object
     * called from {@link com.example.afpromotions.MainActivity}
     */
    public void setData(Promotion promotion) {
        if (promotion != null) {
            this.promotion = promotion;
        }
    }

    // Method to inflate the views with the promotion details
    private void inflate() {
        if (promotion != null) {
            Picasso.with(getActivity())
                    .load(promotion.getImage())
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(promotionImage);
            promotionTitle.setText(promotion.getTitle());
            promotionDescription.setText(promotion.getDescription());
            if (promotion.getFooter() == null) {
                promotionFooter.setVisibility(View.GONE);
            } else {
                setTextViewHTML(promotionFooter, promotion.getFooter());
            }
            shopNow.setText(promotion.getButton().get(0).getTitle());
            shopNowUrl = promotion.getButton().get(0).getTarget();
        }
    }

    // Method to set the text view with the HTML string along with the links
    private void setTextViewHTML(TextView footer, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        footer.setText(strBuilder);
        footer.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // Method to handle the clicks on html links within the text
    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                if(GetConnectivity.isNetworkAvailable(getActivity())){
                    showTerms(span);
                } else {
                    Dialog.showErrorDialog(
                            getString(R.string.no_internet_header),
                            getString(R.string.no_internet_description),
                            getString(R.string.close),
                            getString(R.string.close), // This won't be displayed
                            true,
                            getActivity(),
                            (DialogInterface.OnClickListener) getActivity()
                    );
                }
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    // Shows a webView in a dialog with the terms and conditions associated
    private void showTerms(URLSpan span) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle(getString(R.string.terms_and_conditions));

        WebView webView = new WebView(getActivity());
        webView.loadUrl(span.getURL());
        // Enable Javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                // Inject CSS when page is done loading
                injectCSS(view);
                super.onPageFinished(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        alert.setView(webView);
        alert.setNegativeButton(getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    // Read mobile.css from assets folder and append it to the document head
    private void injectCSS(WebView webView) {
        try {
            InputStream inputStream = getActivity().getAssets().open("mobile.css");
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            String encoded = Base64.encodeToString(buffer, Base64.NO_WRAP);
            webView.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
