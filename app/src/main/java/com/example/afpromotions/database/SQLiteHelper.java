package com.example.afpromotions.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String PROMOTIONS_TABLE = "Promotions";
    public static final String PROMOTION_ID = "_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String FOOTER = "footer";
    public static final String IMAGE_URL = "imageUrl";
    public static final String BUTTON_TITLE = "buttonTitle";
    public static final String BUTTON_TARGET = "buttonTarget";

    private static final String DATABASE_NAME = "Promotions.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE = "CREATE TABLE " + PROMOTIONS_TABLE
                    + "(" + PROMOTION_ID + " integer primary key autoincrement, "
                    + TITLE + " text not null, "
                    + DESCRIPTION + " text not null, "
                    + FOOTER + " text, "
                    + IMAGE_URL + " text not null, "
                    + BUTTON_TITLE + " text not null, "
                    + BUTTON_TARGET + " text not null);";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PROMOTIONS_TABLE);
        onCreate(db);
    }

}
