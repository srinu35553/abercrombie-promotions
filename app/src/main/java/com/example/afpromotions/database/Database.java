package com.example.afpromotions.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.api.model.PromotionButton;

import java.util.ArrayList;
import java.util.List;

public class Database {
    // Database fields
    private SQLiteHelper dbHelper;
    private SQLiteDatabase database;
    private String[] PROMTIONS_TABLE_COLUMNS = {
            SQLiteHelper.PROMOTION_ID,
            SQLiteHelper.TITLE,
            SQLiteHelper.DESCRIPTION,
            SQLiteHelper.FOOTER,
            SQLiteHelper.IMAGE_URL,
            SQLiteHelper.BUTTON_TITLE,
            SQLiteHelper.BUTTON_TARGET
    };

    public Database(Context context) {
        dbHelper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void addPromotions(ArrayList<Promotion> promotions) {
        for (Promotion promotion : promotions) {
            ContentValues values = new ContentValues();
            values.put(SQLiteHelper.TITLE, promotion.getTitle());
            values.put(SQLiteHelper.DESCRIPTION, promotion.getDescription());
            if (promotion.getFooter() != null) {
                values.put(SQLiteHelper.FOOTER, promotion.getFooter());
            }
            values.put(SQLiteHelper.IMAGE_URL, promotion.getImage());
            values.put(SQLiteHelper.BUTTON_TITLE, promotion.getButton().get(0).getTitle());
            values.put(SQLiteHelper.BUTTON_TARGET, promotion.getButton().get(0).getTarget());

            database.insert(SQLiteHelper.PROMOTIONS_TABLE, null, values);
        }
    }

    public void clearDatabase() {
        database.delete(SQLiteHelper.PROMOTIONS_TABLE, null, null);
    }

    public ArrayList<Promotion> getPromotions() {
        ArrayList<Promotion> promotions = new ArrayList();

        Cursor cursor = database.query(SQLiteHelper.PROMOTIONS_TABLE,
                PROMTIONS_TABLE_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Promotion promotion = parsePromotion(cursor);
            promotions.add(promotion);
            cursor.moveToNext();
        }

        cursor.close();
        return promotions;
    }

    private Promotion parsePromotion(Cursor cursor) {
        Promotion promotion = new Promotion();
        promotion.setId((cursor.getInt(0)));
        promotion.setTitle((cursor.getString(1)));
        promotion.setDescription((cursor.getString(2)));
        promotion.setFooter((cursor.getString(3)));
        promotion.setImage((cursor.getString(4)));

        ArrayList<PromotionButton> buttons = new ArrayList<>();
        PromotionButton button = new PromotionButton();
        button.setTitle((cursor.getString(5)));
        button.setTarget((cursor.getString(6)));
        buttons.add(button);

        promotion.setButton(buttons);

        return promotion;
    }
}
