package com.example.afpromotions;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.database.Database;
import com.example.afpromotions.fragments.LaunchFragment;
import com.example.afpromotions.utils.FragmentTestContainerActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class TableViewTest {

    private LaunchFragment fragment;
    private List<Promotion> promotions;

    @Rule
    public ActivityTestRule<FragmentTestContainerActivity> activityRule = new ActivityTestRule(FragmentTestContainerActivity.class);

    @Before
    public void setUp() {
        promotions = activityRule.getActivity().promotions;

        fragment = new LaunchFragment();
        fragment.setData(promotions);

        activityRule.getActivity().addFragment(fragment);
    }

    @Test
    public void testDatabase() throws Exception {
        assertTrue(promotions.size() > 0);
    }

    @Test
    public void testTitlesDisplayed() throws Exception {
        // Checking to see if the title is displayed form the first promotion
        onView(withText(promotions.get(0).getTitle())).check(matches(isDisplayed()));

        // Checking to see if the title is displayed form the second promotion
        onView(withText(promotions.get(1).getTitle())).check(matches(isDisplayed()));
    }
}
