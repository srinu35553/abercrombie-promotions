package com.example.afpromotions;

import android.content.Context;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.example.afpromotions.api.model.Promotion;
import com.example.afpromotions.database.Database;
import com.example.afpromotions.fragments.DetailFragment;
import com.example.afpromotions.fragments.WebViewFragment;
import com.example.afpromotions.utils.Dialog;
import com.example.afpromotions.utils.FragmentTestContainerActivity;
import com.example.afpromotions.utils.GetConnectivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.openLinkWithText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DetailViewTest {

    private DetailFragment fragment;
    private Promotion promotion;
    private Context context;

    @Rule
    public ActivityTestRule<FragmentTestContainerActivity> activityRule = new ActivityTestRule(FragmentTestContainerActivity.class);

    @Before
    public void setUp() {
        // Checking for the first promotion which also has footer
        promotion = activityRule.getActivity().promotions.get(0);

        fragment = new DetailFragment();
        fragment.setData(promotion);

        activityRule.getActivity().addFragment(fragment);
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void testDatabase() throws Exception {
        assertTrue(promotion != null);
    }

    @Test
    public void testDetailsDisplayed() throws Exception {
        // Checking to see if the title is displayed form the first promotion
        onView(withText(promotion.getTitle())).check(matches(isDisplayed()));

        // Checking to see if the description is displayed form the second promotion
        onView(withText(promotion.getDescription())).check(matches(isDisplayed()));

        // Checking to see if the button is displayed form the second promotion
        onView(withText(promotion.getButton().get(0).getTitle())).check(matches(isDisplayed()));
    }

    @Test
    public void testShopNow() throws Exception {
        // Click on see details
        onView(withId(R.id.shop_now)).perform(click());

        if(GetConnectivity.isNetworkAvailable(context)){
            // Check if the terms and conditions are displayed
            onView(withId(R.id.web_view)).check(matches(isDisplayed()));
        } else {
            // Check if the no internet connection dialog is shown
            onView(withText(R.string.no_internet_header)).check(matches(isDisplayed()));
        }
    }
}
