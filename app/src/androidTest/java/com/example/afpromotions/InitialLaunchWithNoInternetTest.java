package com.example.afpromotions;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.test.ActivityInstrumentationTestCase2;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class InitialLaunchWithNoInternetTest extends ActivityInstrumentationTestCase2<SplashScreen> {

    public InitialLaunchWithNoInternetTest() {
        super(SplashScreen.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testDialogShown() throws Exception {
        // Check to see if the dialog is shown
        onView(withText(R.string.no_internet_header)).check(matches(isDisplayed()));
    }
}
